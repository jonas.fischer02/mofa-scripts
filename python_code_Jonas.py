# python code Jonas
# principal component analysis of
## MOFA imputed data and
## kNN imputed data


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.impute import KNNImputer

# kNN imputation
dat_missing = pd.read_csv("mrna_matrix_missing.csv").T
metadat = pd.read_csv("cll_metadata.csv")
genes = pd.read_csv("genes.csv", header=None)

imputer = KNNImputer(n_neighbors=2)
dat = pd.DataFrame(imputer.fit_transform(dat_missing))
dat.index = dat_missing.index

# PCA on kNN imputed data
pca = PCA(n_components=5)
pcs = pca.fit_transform(dat)

datdf = pd.DataFrame(dat, columns=dat.columns.values)
datdf.reset_index(inplace=True)
datdf.rename(columns={'index': 'sample'}, inplace=True)
samples = datdf['sample']

# data wrangling
pcsdf = pd.DataFrame(pcs)
pcsdf = pcsdf.add_prefix("pc")
pcsdf['sample'] = samples
merged = pd.merge(pcsdf, metadat, on="sample", how="inner")
merged['IGHV'] = merged['IGHV'].fillna(2)
merged['trisomy12'] = merged['trisomy12'].fillna(2)

# plots
ighvneg = merged[merged['IGHV'] == 0]
ighvpos = merged[merged['IGHV'] == 1]
ighvnan = merged[merged['IGHV'] == 2]

ighv = [ighvneg, ighvpos, ighvnan]
ighvcol = ['blue','red','grey']
ighvlbl = ['IGHV-','IGHV+','IGHV missing']

trsneg = merged[merged['trisomy12'] == 0]
trspos = merged[merged['trisomy12'] == 1]
trsnan = merged[merged['trisomy12'] == 2]

trs = [trsneg, trspos, trsnan]
trscol = ['blue','red','grey']
trslbl = ['trisomy12-','trisomy12+','trisomy12 missing']

fig, ax = plt.subplots(figsize=(5,5))

for i in range(len(ighv)):
    plt.scatter(ighv[i]['pc0'], ighv[i]['pc1'], c=ighvcol[i], label=ighvlbl[i])
ax.set_xlabel('PC1')
ax.set_ylabel('PC2')
ax.set_title('PCA on kNN imputed mRNA data')
ax.legend()
fig.savefig(f'img/knn_imputed_ighv.png', dpi=300)

fig, ax = plt.subplots(figsize=(5,5))

for i in range(len(trs)):
    ax.scatter(trs[i]['pc0'], trs[i]['pc1'], c=trscol[i], label=trslbl[i])
ax.set_xlabel('PC1')
ax.set_ylabel('PC2')
ax.set_title('PCA on kNN imputed mRNA data')
ax.legend()
fig.savefig(f'img/knn_imputed_trisomy.png', dpi=300)

# same workflow for PCA and visualization of MOFA imputed mRNA values